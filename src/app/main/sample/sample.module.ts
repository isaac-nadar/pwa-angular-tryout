import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { SampleComponent } from './sample.component';
import { AuthGuardService as AuthGuard } from '../authentication/auth-guard.service';
import { CdkTableModule } from '@angular/cdk/table';

import {
  MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatGridListModule,
  MatTableModule, MatTabsModule, MatPaginatorModule, MatNativeDateModule, MatSelectModule,
  MatDatepickerModule, MatDialogModule, MatToolbarModule, MatSortModule, MatCheckboxModule, MatProgressSpinnerModule
} from '@angular/material';

const routes = [
    {
        path     : 'users',
        component: SampleComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    declarations: [
        SampleComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        CdkTableModule,

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTableModule,
        MatTabsModule,
        MatPaginatorModule,
        MatSelectModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatToolbarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatGridListModule,
        MatSortModule,
        MatCheckboxModule
    ],
    exports     : [
        SampleComponent
    ],
    providers : [
      AuthGuard
    ]
})

export class SampleModule
{
}
