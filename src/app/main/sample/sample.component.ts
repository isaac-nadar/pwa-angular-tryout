import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';

// import { ActivatedRoute, Router } from "@angular/router";
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { UserTabService } from "./sample.service";

@Component({
    selector   : 'sample',
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss']
})
export class SampleComponent implements OnInit, AfterViewInit
{
  shipperData: any;
  forwarderData: any;
  sortData = [];
  dataSourceAvailable = false;

  displayedColumns = ['name', 'email', 'number', 'domain', 'created'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  dataSource = new MatTableDataSource();

  constructor(private userService: UserTabService) {

  }

  ngOnInit() {
    this.getShipperList();
    this.getForwarderList();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getShipperList() {
    this.userService.getShipperData().subscribe(
      data => {
        this.shipperData = data;
        this.dataSource.data = this.shipperData.data;
        this.dataSourceAvailable = false;
      },
      err => {
        console.error(err)
        this.dataSourceAvailable = true;
      },
      () => {
        this.shipperData = null;
      }
    );
  }

  getForwarderList() {
    this.userService.getForwarderData().subscribe(
      data => {
        this.forwarderData = data;
      },
      err => console.error(err),
      () => {
        this.forwarderData = null;
      }
    );
  }
}
