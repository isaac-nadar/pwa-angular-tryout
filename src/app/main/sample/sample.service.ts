import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../authentication/auth.service';
import { url } from '../../../global.variables';

@Injectable()

export class UserTabService {
  // public setMilestoneData: any;
  // public store = {};
  // public carrierData = null;


  // private serviceUrl = url + 'subscription';
  private url = url.toString();

  // private serviceUrl = 'https://api.myjson.com/bins/125nuf';

  constructor(public http: HttpClient, public auth: AuthService) { }

  getShipperData() {
    return this.http.get(this.url + 'api/shipper');
  }

  getForwarderData() {
    return this.http.get(this.url + 'api/forwarder');
  }

}
