import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tokenNotExpired } from 'angular2-jwt';
import { url } from '../../../global.variables';

@Injectable()
export class AuthService {

  domain = url.toString(); // Development Domain - Not Needed in Production
  authToken;
  user;
  options;
  public store = [];

  constructor(public http: HttpClient) {
   }

  // Function to create headers, add token, to be used in HTTP requests
  createAuthenticationHeaders() {
    this.loadToken(); // Get token so it can be attached to headers
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': this.authToken
      })
    };
    return httpOptions;
  }

  // Function to get token from client local storage
  loadToken() {
    this.authToken = localStorage.getItem('token');
  }

  getLink(data) {
    return this.http.post(this.domain + 'share', data, this.createAuthenticationHeaders());
  }

  getSharedData() {
    return this.http.get(this.domain + 'share', this.createAuthenticationHeaders());
  }

  login(user) {
    // return this.http.post(this.domain + '/authentication/login', user).map(res => res.json());
    return this.http.post(this.domain + 'oauth/token', user);
  }

  addSub(sub) {
    return this.http.post(this.domain + 'subscription', sub, this.createAuthenticationHeaders());
  }

  getOptions() {
    return this.http.get(this.domain + 'carrier', this.createAuthenticationHeaders());
  }

  saveNewDate(payload) {
    return this.http.put(this.domain + 'share/' + payload.uuid, payload, this.createAuthenticationHeaders());
  }

  revokeLink(id, data) {
    return this.http.put(this.domain + 'share/' + id, data, this.createAuthenticationHeaders());
  }

  logout() {
    this.authToken = null; // Set token to null
    this.user = null; // Set user to null
    localStorage.clear(); // Clear local storage
  }

  // Function to store user's data in client local storage
  storeUserData(token, user) {
    localStorage.setItem('token', token); // Set token in local storage
    localStorage.setItem('user', user); // Set user in local storage as string
    this.authToken = token; // Assign token to be used elsewhere
    this.user = user; // Set user to be used elsewhere
  }

  loggedIn() {
    return tokenNotExpired();
  }
}
