import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // let host_name = window.location.hostname.replace('http://', '').replace('https://', '').replace('www.', '').split(/[/?#]/)[0];
    // host_name = ((host_name.indexOf('localhost')) > -1 || (host_name.indexOf('192.168')) > -1 || (host_name.indexOf('35.154') > -1)) ? 'fwda.stagingship.freightbro.com' : host_name;
    let host_name = 'fwda.stagingship.freightbro.com';

    this.auth.loadToken();

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.authToken}`,
        'referer-domain': host_name
      }
    });
    return next.handle(request);
  }
}
