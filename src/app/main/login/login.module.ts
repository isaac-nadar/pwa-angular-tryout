import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseLogin2Component } from './login.component';
import { NotAuthGuardService as NotAuthGuard } from '../authentication/not-auth-guard.service';

const routes = [
    {
        path     : 'login',
        component: FuseLogin2Component,
        canActivate: [NotAuthGuard]
    }
];

@NgModule({
    declarations: [
        FuseLogin2Component
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,

        FuseSharedModule
    ],
    providers: [
        // NotAuthGuard
    ]
})
export class Login2Module
{
}
