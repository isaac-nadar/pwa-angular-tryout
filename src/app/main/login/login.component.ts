import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from '../authentication/auth.service';
import { Router } from '@angular/router';


@Component({
    selector: 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: fuseAnimations
})
export class FuseLogin2Component implements OnInit {
    loginForm: FormGroup;
    loginFormErrors: any;
    data: any;
    loginError = '';
    loginApiCall = false;

    constructor(
      private _fuseConfigService: FuseConfigService,
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router
    ) {
      this._fuseConfigService.config = {
        layout: {
          navbar: {
            hidden: true
          },
          toolbar: {
            hidden: true
          },
          footer: {
            hidden: true
          },
          sidepanel: {
            hidden: true
          }
        }
      };

        this.loginFormErrors = {
            username: {},
            password: {}
        };
    }

    disableButton: boolean;

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required]],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }

    onLoginFormValuesChanged() {
        for (const field in this.loginFormErrors) {
            if (!this.loginFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }

    onLoginSubmit() {
        this.loginError = '';
        this.disableButton = true;
        const user = {
            "client_id": "2",
            "client_secret": "SyMQ2QaohjBbgfpTZPQiy6EdqonlmIAx1ZEqCPt7",
            "grant_type": "password",
            "scope": "",
            username: this.loginForm.get('username').value, // Username input field
            password: this.loginForm.get('password').value // Password input field
          };
        this.loginApiCall = true;
        this.authService.login(user).subscribe(data => {
            // Check if response was a success or error
            this.data = data;
            this.loginApiCall = false;
            const tokenData = { value: data, time: new Date().getTime() };
            if (this.data){
                this.disableButton = false;
                localStorage.clear();
                localStorage.setItem('authToken', JSON.stringify(tokenData));
                this.authService.storeUserData(this.data.access_token, 'User');
                this.router.navigate(['/user']);
            } else {
                this.disableButton = false;
            }
        },
          err => {
            console.error(err)
            this.loginApiCall = false;
            this.loginError = err.error.message;
          });
    }
}
